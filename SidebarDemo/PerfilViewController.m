//
//  PerfilViewController.m
//  SidebarDemo
//
//  Created by Victor Rosas on 6/4/15.
//  Copyright (c) 2015 AppCoda. All rights reserved.
//

#import "PerfilViewController.h"
#import "SWRevealViewController.h"
@interface PerfilViewController ()
@end
@implementation PerfilViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Perfil";
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)sliderpagocambio:(id)sender {
    self.lblpagorenta.text = [NSString stringWithFormat:@"%.02f", (self.sliderpagorenta.value * 50)];
}

- (IBAction)switchcambio:(id)sender {
    UISwitch *mySwitch = (UISwitch *)sender;
    if ([mySwitch isOn]) {
        self.lblcasa.text = @"Tengo casa";
    } else {
        self.lblcasa.text = @"Busco casa";
    }
}

- (IBAction)touchbtngaming:(id)sender {
    if (self.btngaming.selected == YES) {
        [self.btngaming setSelected:NO];
       [self.btngaming setBackgroundImage:[UIImage imageNamed:@"Controller-50.png"] forState:UIControlStateNormal];
    }
    else{
    [self.btngaming setSelected:YES];
    
        [self.btngaming setBackgroundImage:[UIImage imageNamed:@"Controller Filled-50.png"] forState:UIControlStateSelected];}
}
- (IBAction)touchbtnbeer:(id)sender {
    if (self.btnbeer.selected == YES) {
        [self.btnbeer setSelected:NO];
        [self.btnbeer setBackgroundImage:[UIImage imageNamed:@"Beer Bottle-50.png"] forState:UIControlStateNormal];
    }
    else{
        [self.btnbeer setSelected:YES];
        
        [self.btnbeer setBackgroundImage:[UIImage imageNamed:@"Beer Bottle Filled-50.png"] forState:UIControlStateSelected];}
}
- (IBAction)touchbtnpot:(id)sender {
    if (self.btnpot.selected == YES) {
        [self.btnpot setSelected:NO];
        [self.btnpot setBackgroundImage:[UIImage imageNamed:@"Cooking Pot-50.png"] forState:UIControlStateNormal];
    }
    else{
        [self.btnpot setSelected:YES];
        
        [self.btnpot setBackgroundImage:[UIImage imageNamed:@"Cooking Pot Filled-50.png"] forState:UIControlStateSelected];}
}

- (IBAction)touchbtnsmoke:(id)sender {
    if (self.btnsmoke.selected == YES) {
        [self.btnsmoke setSelected:NO];
        [self.btnsmoke setBackgroundImage:[UIImage imageNamed:@"Smoking-50.png"] forState:UIControlStateNormal];
    }
    else{
        [self.btnsmoke setSelected:YES];
        
        [self.btnsmoke setBackgroundImage:[UIImage imageNamed:@"Smoking Filled-50.png"] forState:UIControlStateSelected];}
}
- (IBAction)touchbtnpet:(id)sender {
    if (self.btnpet.selected == YES) {
        [self.btnpet setSelected:NO];
        [self.btnpet setBackgroundImage:[UIImage imageNamed:@"No Animals-50.png"] forState:UIControlStateNormal];
    }
    else{
        [self.btnpet setSelected:YES];
        
        [self.btnpet setBackgroundImage:[UIImage imageNamed:@"No Animals Filled-50.png"] forState:UIControlStateSelected];}
}
- (IBAction)touchbtncar:(id)sender {
    if (self.btncar.selected == YES) {
        [self.btncar setSelected:NO];
        [self.btncar setBackgroundImage:[UIImage imageNamed:@"Garage-50.png"] forState:UIControlStateNormal];
    }
    else{
        [self.btncar setSelected:YES];
        
        [self.btncar setBackgroundImage:[UIImage imageNamed:@"Garage Filled-50.png"] forState:UIControlStateSelected];}
}
- (IBAction)touchbtnmop:(id)sender {
    if (self.btnmop.selected == YES) {
    [self.btnmop setSelected:NO];
    [self.btnmop setBackgroundImage:[UIImage imageNamed:@"Housekeeping-50.png"] forState:UIControlStateNormal];
}
else{
    [self.btnmop setSelected:YES];
    
    [self.btnmop setBackgroundImage:[UIImage imageNamed:@"Housekeeping Filled-50.png"] forState:UIControlStateSelected];}
}
- (IBAction)touchbtnfood:(id)sender {
    if (self.btnfood.selected == YES) {
        [self.btnfood setSelected:NO];
        [self.btnfood setBackgroundImage:[UIImage imageNamed:@"Restaurant Building-50.png"] forState:UIControlStateNormal];
    }
    else{
        [self.btnfood setSelected:YES];
        
        [self.btnfood setBackgroundImage:[UIImage imageNamed:@"Restaurant Building Filled-50.png"] forState:UIControlStateSelected];}
}
- (IBAction)touchbtndisc:(id)sender {
    if (self.btndisc.selected == YES) {
        [self.btndisc setSelected:NO];
        [self.btndisc setBackgroundImage:[UIImage imageNamed:@"Wheelchair-50.png"] forState:UIControlStateNormal];
    }
    else{
        [self.btndisc setSelected:YES];
        
        [self.btndisc setBackgroundImage:[UIImage imageNamed:@"Wheelchair Filled-50.png"] forState:UIControlStateSelected];}
}
- (IBAction)touchbtnchild:(id)sender {
    if (self.btnchild.selected == YES) {
        [self.btnchild setSelected:NO];
        [self.btnchild setBackgroundImage:[UIImage imageNamed:@"Keep Away From Children-50.png"] forState:UIControlStateNormal];
    }
    else{
        [self.btnchild setSelected:YES];
        
        [self.btnchild setBackgroundImage:[UIImage imageNamed:@"Keep Away From Children Filled-50.png"] forState:UIControlStateSelected];}
}
- (IBAction)cambioimagen:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.delegate = self;
    [self presentViewController:picker animated:YES completion:NULL];
}

- (IBAction)tnameend:(id)sender {
}

- (IBAction)tageend:(id)sender {
}

- (IBAction)tcdend:(id)sender {
}

- (IBAction)tocend:(id)sender {
}

- (void)imagePickerController:(UIImagePickerController *)picker
        didFinishPickingImage:(UIImage *)image
                  editingInfo:(NSDictionary *)editingInfo
{
    self.imagen.image = image;
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(BOOL) textFieldShouldReturn: (UITextField *) textField {
    [textField resignFirstResponder];
    return YES;
}


@end
