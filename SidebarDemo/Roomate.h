//
//  Roomate.h
//  SidebarDemo
//
//  Created by Victor Rosas on 6/5/15.
//  Copyright (c) 2015 AppCoda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Roomate : NSObject{
NSString *imagename;
NSString *nombre;
NSString *edad;
NSString *ocupacion;
NSString *ciudad;
BOOL casa;
NSInteger maxpago;
BOOL game;
BOOL beer;
BOOL pot;
BOOL cig;
BOOL dog;
BOOL car;
BOOL mop;
BOOL food;
BOOL wheelchair;
BOOL baby;
NSString *acercademi;
}

-(id)init:(NSString*)imagename pnombre:(NSString*)nombre pedad:(NSString*)edad pocupacion:(NSString*)ocupacion pciudad:(NSString*)ciudad pcasa:(BOOL)casa
 pmaxpago:(NSInteger)maxpago pgame:(BOOL)game pbeer:(BOOL)beer ppot:(BOOL)pot pcig:(BOOL)cig pdog:(BOOL)dog pcar:(BOOL)car pmop:(BOOL)mop pfood:(BOOL)food pwheelchair:(BOOL)wheelchair pbaby:(BOOL)baby pacerca:(NSString*)acercademi;

@end
