//
//  PerfilViewController.h
//  SidebarDemo
//
//  Created by Victor Rosas on 6/4/15.
//  Copyright (c) 2015 AppCoda. All rights reserved.
//

#import "ViewController.h"

@interface PerfilViewController : ViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imagen;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (weak, nonatomic) IBOutlet UISlider *sliderpagorenta;
@property (weak, nonatomic) IBOutlet UILabel *lblpagorenta;
@property (weak, nonatomic) IBOutlet UISwitch *switchcasa;
@property (weak, nonatomic) IBOutlet UILabel *lblcasa;
- (IBAction)sliderpagocambio:(id)sender;
- (IBAction)switchcambio:(id)sender;
- (IBAction)touchbtngaming:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btngaming;
@property (weak, nonatomic) IBOutlet UIButton *btnbeer;
- (IBAction)touchbtnbeer:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnpot;
- (IBAction)touchbtnpot:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnsmoke;
- (IBAction)touchbtnsmoke:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnpet;
- (IBAction)touchbtnpet:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btncar;
- (IBAction)touchbtncar:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnmop;
- (IBAction)touchbtnmop:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnfood;
- (IBAction)touchbtnfood:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btndisc;
- (IBAction)touchbtndisc:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnchild;
- (IBAction)touchbtnchild:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *txtacercademi;
@property (weak, nonatomic) IBOutlet UITextField *txtname;
@property (weak, nonatomic) IBOutlet UITextField *txtedad;
@property (weak, nonatomic) IBOutlet UITextField *txtwork;
@property (weak, nonatomic) IBOutlet UITextField *txtciudad;
- (IBAction)cambioimagen:(id)sender;
- (IBAction)tnameend:(id)sender;
- (IBAction)tageend:(id)sender;
- (IBAction)tcdend:(id)sender;
- (IBAction)tocend:(id)sender;

@end
